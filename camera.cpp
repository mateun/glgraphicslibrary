#include "pch.h"
#include "camera.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/mat4x4.hpp>

using namespace glm;

ggraphics::Camera::Camera()
{
	
}

ggraphics::Camera::Camera(vec3 position, vec3 center, vec3 up, float hfov)
{
	_pos = position;
	_view = lookAt(position, center, up);
	_forward = normalize(center - position);
	_right = normalize(cross(_forward, vec3(0, 1, 0)));
	_projection = perspectiveFov<float>(hfov, 8, 5, 0.1, 300);
	
}

void ggraphics::Camera::updateMatrices()
{
	
}

void ggraphics::Camera::strafeH(bool left)
{
	float dir = 1;
	if (left) dir = -1;

	
	
	_pos += _right * dir * 0.5f;
	vec3 center = _pos + _forward;
	_view = lookAt(_pos, center, vec3(0, 1, 0));
}

void ggraphics::Camera::strafeV(bool up)
{
	float dir = 1;
	if (up) dir = -1;
	vec3 fw = normalize(cross(_right, vec3(0, 1, 0)));
	_pos += fw * dir * 0.5f;
	vec3 center = _pos + _forward;
	_view = lookAt(_pos, center, vec3(0, 1, 0));
}

void ggraphics::Camera::zoom(bool in)
{
	float dir = 1;
	if (!in) dir = -1;
	_pos += normalize(_forward) * 0.5f * dir;
	vec3 center = _pos + _forward;
	_view = lookAt(_pos, center, vec3(0, 1, 0));
}

void ggraphics::Camera::rotate(bool cw)
{
	float dir = 1;
	if (!cw) dir = -1;
	mat4 rotmatrix = glm::rotate<float>(mat4(1), 0.01 * dir, vec3(0, 1, 0));
	_forward = rotmatrix * vec4(_forward, 1);
	_right = normalize(cross(_forward, vec3(0, 1, 0)));
	
	vec3 center = _pos + _forward;
	_view = lookAt(_pos, center, vec3(0, 1, 0));
}

