#pragma once
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

using namespace glm;

namespace ggraphics {
	class Camera
	{
	public:
		Camera();
		Camera(vec3 position, vec3 lookAt, vec3 up, float hfov);
		void updateMatrices();
		mat4 model() { return _model;  }
		mat4 view() { return _view; }
		mat4 projection() { return _projection; }
		void strafeH(bool left);
		void strafeV(bool up);
		void zoom(bool in);
		void rotate(bool cw);
		

	private:
		vec3 _forward;
		vec3 _right;
		vec3 _pos;
		vec3 _lookat;
		vec3 _up;
		mat4 _model = mat4(1);
		mat4 _view = mat4(1);
		mat4 _projection = mat4(1);
	};
}
