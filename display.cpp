#include "pch.h"
#include "display.h"


ggraphics::Display::Display(int w, int h, bool fullscreen)
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		SDL_Log("error initializing!");
		return;
	}

	_window = SDL_CreateWindow("gDisplay 0.0.1", 300, 100, w, h, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
}


ggraphics::Display::~Display()
{
}

bool ggraphics::Display::pollEvent(SDL_Event* event)
{
	return SDL_PollEvent(event);
	
}

SDL_Window* ggraphics::Display::getWindow()
{
	return _window;
}