#pragma once
#include <SDL.h>

namespace ggraphics {
	
	class Display {

	public:
		Display(int w, int h, bool fullscreen);
		virtual ~Display();
		bool pollEvent(SDL_Event* event);
		SDL_Window* getWindow();

	private:
		SDL_Window* _window;

	};
}

