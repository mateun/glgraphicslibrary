#include "pch.h"
#include "camera.h"
#include "glrenderer.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <gl/glew.h>
#include <glm/gtc/type_ptr.hpp>

#include "model.h"


ggraphics::GLRenderer::GLRenderer(const Display& display) : _display(display)
{
	_renderer = SDL_CreateRenderer(_display.getWindow(), -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	_clearColor = new float[4];

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);



	SDL_GLContext glContext = SDL_GL_CreateContext(_display.getWindow());

	const GLubyte* version = glGetString(GL_VERSION);
	SDL_Log("GL Version: %s\n", version);
	version = glGetString(GL_SHADING_LANGUAGE_VERSION);
	SDL_Log("Shader version: %s\n", version);

	bool gameRunning = true;
	SDL_Event event;
	glClearColor(0, 0, 0, 1);

	glewExperimental = GL_TRUE;
	GLenum error = glewInit();
	if (GLEW_OK != error) {
		SDL_Log("glew init failed %s\n", glewGetErrorString(error));
		return;
	}

	GLint majVer, minVer;
	glGetIntegerv(GL_MAJOR_VERSION, &majVer);
	glGetIntegerv(GL_MINOR_VERSION, &minVer);
	SDL_Log("GL Version %d/%d\n", majVer, minVer);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);

	fullScreenQuadMeshVAO = createVAO({ -1, 1, 0 , -1, -1, 0 ,  1, -1, 0 ,  1, 1, 0 }, { 0, 0 ,  0, 1 ,  1, 1 ,  1, 0 },
		{ 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1 }, { 0, 1, 2, 0, 2, 3 });
}

GLuint ggraphics::GLRenderer::createVAO(vector<float> positions, vector<float> uvs, vector<float> normals,
	vector<GLuint> indices)
{
	GLuint vao;
	glCreateVertexArrays(1, &vao);
	glBindVertexArray(vao);

	GLuint vb_pos;
	glGenBuffers(1, &vb_pos);
	glBindBuffer(GL_ARRAY_BUFFER, vb_pos);
	glBufferData(GL_ARRAY_BUFFER, positions.size() * sizeof(GL_FLOAT), positions.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)nullptr);
	glEnableVertexAttribArray(0);

	GLuint vb_uvs;
	glGenBuffers(1, &vb_uvs);
	glBindBuffer(GL_ARRAY_BUFFER, vb_uvs);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(GL_FLOAT), uvs.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)nullptr);
	glEnableVertexAttribArray(1);

	GLuint vb_normals;
	glGenBuffers(1, &vb_normals);
	glBindBuffer(GL_ARRAY_BUFFER, vb_normals);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(GL_FLOAT), normals.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)nullptr);
	glEnableVertexAttribArray(2);

	GLuint vb_indices;
	glGenBuffers(1, &vb_normals);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vb_normals);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GL_UNSIGNED_INT), indices.data(), GL_STATIC_DRAW);
	
	GLuint glError = 0;

	if ((glError = glGetError()) != 0)
	{
		SDL_Log("gl error0: %d", glError);
	}


	return vao;
}

void ggraphics::GLRenderer::setClearColor(float r, float g, float b, float a)
{
	_clearColor[0] = r;
	_clearColor[1] = g;
	_clearColor[2] = b;
	_clearColor[3] = a;
}

void ggraphics::GLRenderer::clearBackbuffer()
{
	glClearColor(_clearColor[0], _clearColor[1], _clearColor[2], _clearColor[3]);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void ggraphics::GLRenderer::clearBackbuffer(float r, float g, float b, float a)
{
	glClearColor(r,g,b,a);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void ggraphics::GLRenderer::present()
{

	SDL_GL_SwapWindow(_display.getWindow());
}

GLuint ggraphics::GLRenderer::createFragmentShader(const std::string& fileName)
{
	return createShader(fileName, GL_FRAGMENT_SHADER);
}

GLuint ggraphics::GLRenderer::createVertexShader(const std::string& fileName)
{
	return createShader(fileName, GL_VERTEX_SHADER);
}

GLuint ggraphics::GLRenderer::createShader(const std::string& fileName, GLint type)
{
	GLuint vshader = glCreateShader(type);
	const std::string vsstring = readCodeFromFile(fileName);
	const GLchar* vssource_char = vsstring.c_str();

	glShaderSource(vshader, 1, &vssource_char, NULL);
	glCompileShader(vshader);
	GLint compileStatus;
	glGetShaderiv(vshader, GL_COMPILE_STATUS, &compileStatus);
	SDL_Log("shader compile status: %d", compileStatus);
	if (GL_FALSE == compileStatus) {
		SDL_Log("problem with shader!!");
		SDL_Log("shader source: %s", vssource_char);

		GLint logSize = 0;
		glGetShaderiv(vshader, GL_INFO_LOG_LENGTH, &logSize);
		std::vector<GLchar> errorLog(logSize);
		glGetShaderInfoLog(vshader, logSize, &logSize, &errorLog[0]);
		SDL_Log("error: %s", errorLog.data());
		glDeleteShader(vshader);
		
		exit(1);
	}

	return vshader;
}

const std::string ggraphics::GLRenderer::readCodeFromFile(const std::string& file) {
	std::ifstream inFile(file, std::ios::in);
	if (!inFile) {
		return 0;
	}

	std::ostringstream code;
	while (inFile.good()) {
		int c = inFile.get();
		if (!inFile.eof()) code << (char)c;
	}
	inFile.close();

	return code.str();
}

void ggraphics::GLRenderer::draw(GLuint vao, mat4 modelMatrix, mat4 viewMatrix, mat4 projectionMatrix, GLuint shaderProgram, int numberOfVertices, GLuint texture, GLint* lit)
{
	GLuint glError = 0;
	
	if ((glError = glGetError()) != 0)
	{
		SDL_Log("gl error0: %d", glError);
	}

	
	glUseProgram(shaderProgram);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);

	GLuint modelIndex = glGetUniformLocation(shaderProgram, "mat_model");
	GLuint viewIndex = glGetUniformLocation(shaderProgram, "mat_view");
	GLuint projectIndex = glGetUniformLocation(shaderProgram, "mat_proj");
	
	glUniformMatrix4fv(modelIndex, 1, false, glm::value_ptr(modelMatrix));
	glUniformMatrix4fv(viewIndex, 1, false, glm::value_ptr(viewMatrix));
	glUniformMatrix4fv(projectIndex, 1, false, glm::value_ptr(projectionMatrix));
	glUniform1iv(1, 1, lit);

	

	if ((glError = glGetError()) != 0)
	{
		SDL_Log("gl error1: %d", glError);
	}

	
	glBindVertexArray(vao);

	if ((glError = glGetError()) != 0)
	{
		SDL_Log("gl error2: %d", glError);
	}
	
	glDrawElements(GL_TRIANGLES, numberOfVertices, GL_UNSIGNED_INT, (void*)0);
	
	if((glError = glGetError()) != 0)
	{
		SDL_Log("gl error3: %d", glError);
	}

	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);
	glUseProgram(0);
}

void ggraphics::GLRenderer::drawFullScreenQuad(GLuint shaderProgram, GLuint texture)
{
	GLuint glError = 0;

	if ((glError = glGetError()) != 0)
	{
		SDL_Log("gl error0: %d", glError);
	}


	glUseProgram(shaderProgram);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);

	GLuint modelIndex = glGetUniformLocation(shaderProgram, "mat_model");
	GLuint viewIndex = glGetUniformLocation(shaderProgram, "mat_view");
	GLuint projectIndex = glGetUniformLocation(shaderProgram, "mat_proj");

	glUniformMatrix4fv(modelIndex, 1, false, glm::value_ptr(mat4(1)));
	glUniformMatrix4fv(viewIndex, 1, false, glm::value_ptr(mat4(1)));
	glUniformMatrix4fv(projectIndex, 1, false, glm::value_ptr(mat4(1)));
	GLint lit = 0;
	glUniform1iv(1, 1, &lit);

	if ((glError = glGetError()) != 0)
	{
		SDL_Log("gl error1: %d", glError);
	}

	glBindVertexArray(fullScreenQuadMeshVAO);

	if ((glError = glGetError()) != 0)
	{
		SDL_Log("gl error2: %d", glError);
	}

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void*)0);

	if ((glError = glGetError()) != 0)
	{
		SDL_Log("gl error3: %d", glError);
	}

	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);
	glUseProgram(0);
}

GLuint ggraphics::GLRenderer::createProgram(GLuint vertexShader, GLuint fragmentShader)
{
	GLuint p= glCreateProgram();
	glAttachShader(p, vertexShader);
	glAttachShader(p, fragmentShader);
	glLinkProgram(p);
	GLint compileStatus;
	glGetProgramiv(p, GL_LINK_STATUS, &compileStatus);

	if (GL_FALSE == compileStatus) {
		SDL_Log("problem with shaderlinking!!");

		GLint maxLength = 0;
		glGetProgramiv(p, GL_INFO_LOG_LENGTH, &maxLength);

		std::vector<GLchar> infoLog(maxLength);
		glGetProgramInfoLog(p, maxLength, &maxLength, &infoLog[0]);

		SDL_Log("link error: %s", infoLog.data());

		// We don't need the program anymore.
		glDeleteProgram(p);
		// Don't leak shaders either.
		glDeleteShader(vertexShader);
		glDeleteShader(fragmentShader);


		
		exit(1);
	}
	
	GLenum err = glGetError();
	if (err != 0) {
		SDL_Log("error: %d", err);
		exit(1);
	}
	SDL_Log("linkstatus: %d", compileStatus);
	return p;

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
}

GLuint ggraphics::GLRenderer::createFramebuffer(int width, int height, GLuint* colorTexture, GLuint* depthTexture)
{
	GLuint _fbHandle;
	glGenFramebuffers(1, &_fbHandle);
	glBindFramebuffer(GL_FRAMEBUFFER, _fbHandle);
	GLuint colTex;
	glGenTextures(1, &colTex);
	glBindTexture(GL_TEXTURE_2D, colTex);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, width, height);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	GLuint depthTex;
	glGenTextures(1, &depthTex);
	glBindTexture(GL_TEXTURE_2D, depthTex);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_DEPTH_COMPONENT24, width, height);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, colTex, 0);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTex, 0);

	GLint result;
	glGetFramebufferAttachmentParameteriv(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_FRAMEBUFFER_ATTACHMENT_DEPTH_SIZE, &result);
	if (result == 0) {
		GLenum error = glGetError();
		SDL_Log("Error in FBO! %d", error);
	}

	GLenum draw_buffers[] = { GL_COLOR_ATTACHMENT0 };
	//glDrawBuffers(1, draw_buffers);

	*colorTexture = colTex;
	if (depthTexture != nullptr) *depthTexture = depthTex;
	
	/*_colorTexture._textureHandle = colTex;
	_colorTexture.w = width;
	_colorTexture.h = height;
	_depthTexture._textureHandle = depthTex;
	_depthTexture.w = width;
	_depthTexture.h = height;*/

	GLenum err = glGetError();
	if (err != 0) SDL_Log("error at fb: %d", err);

	GLenum completeness = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (completeness != GL_FRAMEBUFFER_COMPLETE) {
		SDL_Log("error with FBO completeness!");
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	return _fbHandle;
}

