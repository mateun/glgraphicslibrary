#pragma once
#include "display.h"
#include <gl/glew.h>
#include <vector>
#include <string>
#include <glm/mat4x4.hpp>

using namespace std;
using namespace glm;

namespace ggraphics {
	class Mesh;

	class GLRenderer {

	public:
		GLRenderer(const Display& display);
		//virtual ~GLRenderer();

		void setClearColor(float r, float g, float b, float a);
		void clearBackbuffer();
		void clearBackbuffer(float r, float g, float b, float a);
		void clearRenderTarget(Uint8 r, Uint8 g, Uint8 b, Uint8 a);
		GLuint createVAO(vector<float> positions, vector<float> uvs, vector<float> normals, vector<GLuint> indices);
		void draw(GLuint vao, mat4 modelMatrix, mat4 viewMatrix, mat4 projectionMatrix, GLuint shaderProgram, int numberOfVertices, GLuint texture, GLint* lit);
		void drawFullScreenQuad(GLuint shaderProgram, GLuint texture);
		void present();
		SDL_Renderer* getSDLRenderer() { return _renderer; }
		GLuint createVertexShader(const std::string& fileName);
		GLuint createFragmentShader(const std::string& fileName);
		GLuint createProgram(GLuint vertexShader, GLuint fragmentShader);
		GLuint createFramebuffer(int w, int h, GLuint* colTex, GLuint* depthTex);

	private:
		const std::string readCodeFromFile(const std::string& file);
		GLuint createShader(const std::string& fileName, GLint type);

	private:
		Display _display;
		SDL_Renderer* _renderer;
		float* _clearColor;
		GLuint fullScreenQuadMeshVAO;

	};

}

