#include "pch.h"
#include "gmath.h"

#include <SDL.h>
#include <glm/glm.hpp>

using namespace glm;

bool rayIntersectsTriangle(vec3 tp1, vec3 tp2, vec3 tp3, vec3 rayDir, vec3 rayOrigin)
{
	// First we construct a plane where the triangle lives in.
	vec3 planeNormal = normalize(cross((tp2 - tp1), (tp3 - tp1)));
	SDL_Log("planeNormal: %.3f %.3f %.3f", planeNormal.x, planeNormal.y, planeNormal.z);
	
	// If the plane normal and our ray direction are normal to each other,
	// the ray must be parallel to the plane, only intersecting at infinity. 
	if (dot(planeNormal, rayDir) == 0)
	{
		return false;
	}

	float d = dot(planeNormal, tp1);
	SDL_Log("plane d: %.3f", d);


	float t = (d - dot(planeNormal, rayOrigin)) / dot(planeNormal, rayDir);
	vec3 intersectionPoint = rayOrigin + rayDir * t;
	SDL_Log("intersectionPoint: %.3f %.3f %.3f", intersectionPoint.x, intersectionPoint.y, intersectionPoint.z);

	// Check to see if the point is within the triangle
	if (dot(planeNormal, cross(tp2 - tp1, intersectionPoint - tp1)) < 0) return false;
	if (dot(planeNormal, cross(tp3 - tp2, intersectionPoint - tp2)) < 0) return false;
	if (dot(planeNormal, cross(tp1 - tp3, intersectionPoint - tp3)) < 0) return false;

	return true;
}

