#pragma once
#include <glm/vec3.hpp>

using namespace glm;

bool rayIntersectsTriangle(vec3 tp1, vec3 tp2, vec3 tp3, vec3 rayDir, vec3 rayOrigin);
