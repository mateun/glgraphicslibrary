#pragma once
#include <gl/glew.h>

#include "material.h"
#include <glm/glm.hpp>

using namespace glm;
using namespace std;

namespace ggraphics
{
	class Mesh
	{
	public:
		Mesh()
		{
			material = new Material();
			material->texture = nullptr;
		}
		Material* material;
		vector<vec3> positions;
		vector<vec3> normals;
		vector<vec2> uvs;
		vector<GLuint> indices;
		GLuint vao;
	};
}

