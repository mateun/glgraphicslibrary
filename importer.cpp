#include "pch.h"
#include "importer.h"
#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags
#include <glm/gtc/type_ptr.hpp>
#include <SDL.h>
#include <string>


template<typename T, typename V>
vector<T> valuesFromVec(vector<V> vecs, int cardinality)
{
	vector<T> values;
	for (auto vec : vecs)
	{

		values.push_back(vec.x);
		values.push_back(vec.y);
		if (cardinality > 2)
		{
			values.push_back(value_ptr(vec)[2]);
		}
	}

	return values;
}


std::string getModelBasePath(const std::string& modelFullPath)
{
	char* theString = (char*)malloc(modelFullPath.size()+1);
	char delimiter[] = "/";
	char* ptr;
	char* nextToken;
	modelFullPath.copy(theString, modelFullPath.size()+1);
	
	// initialisieren und ersten Abschnitt erstellen
	ptr = strtok_s(theString, delimiter, &nextToken);

	std::vector<std::string> pathParts;
	while (ptr != NULL) {
		pathParts.push_back(std::string(ptr));
		ptr = strtok_s(NULL, delimiter, &nextToken);
	}
	std::string basePath = "";
	for (int i = 0; i < pathParts.size()-1; i++)
	{
		basePath += pathParts[i] + "/";
	}

	free(theString);
	return basePath;
}


ggraphics::Model importFromFile(const std::string& fileName, ggraphics::GLRenderer& glRenderer)
{

	Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile(fileName,
        aiProcess_CalcTangentSpace |
        aiProcess_Triangulate |
        aiProcess_JoinIdenticalVertices |
        aiProcess_SortByPType
		| aiProcess_FlipUVs);

	if (scene == nullptr)
	{
        SDL_Log("error importing scene, exiting!");
        exit(1);
	}


    ggraphics::Model model;
	
	
	for (int i = 0; i < scene->mNumMeshes; i++) {
		auto* mesh = scene->mMeshes[i];
		ggraphics::Mesh myMesh;
		
		for (int x = 0; x < mesh->mNumVertices;x++)
		{
			aiVector3D pos = mesh->mVertices[x];
			myMesh.positions.push_back({ pos.x, pos.y, pos.z });

			if (mesh->HasNormals())
			{
				aiVector3D normal = mesh->mNormals[x];
				myMesh.normals.push_back({ normal.x, normal.y, normal.z });
			}

			if (mesh->HasTextureCoords(0))
			{
				aiVector3D uv = mesh->mTextureCoords[0][x];
				myMesh.uvs.push_back({ uv.x, uv.y});
			}
		}

		for (int f = 0; f < mesh->mNumFaces; f++)
		{
			aiFace face = mesh->mFaces[f];
			myMesh.indices.push_back(face.mIndices[0]);
			myMesh.indices.push_back(face.mIndices[1]);
			myMesh.indices.push_back(face.mIndices[2]);
		}
		myMesh.vao = glRenderer.createVAO(valuesFromVec<float, vec3>(myMesh.positions, 3), valuesFromVec<float, vec2>(myMesh.uvs, 2),
			valuesFromVec<float, vec3>(myMesh.normals, 3), myMesh.indices);

		// Material and texture import
		aiMaterial* aimat = scene->mMaterials[mesh->mMaterialIndex];
		ggraphics::Material* material = new ggraphics::Material();
		material->name = std::string(aimat->GetName().data);
		aiString texturePath;
		std::string basePath = getModelBasePath(fileName);
		aiReturn ret = aimat->GetTexture(aiTextureType::aiTextureType_DIFFUSE, 0, &texturePath, NULL, NULL, NULL, NULL, NULL);
		material->texture = new ggraphics::Texture(std::string(basePath + "/" + texturePath.C_Str()), vec3(0));
		myMesh.material = material;
		
		model.meshes.push_back(myMesh);
		
	}

	return model;

}