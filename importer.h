#pragma once
#include <string>
#include "model.h"


ggraphics::Model importFromFile(const std::string& fileName, ggraphics::GLRenderer& renderer);
