#pragma once
#include <glm/glm.hpp>
#include "texture.h"

using namespace glm;
using namespace std;

namespace  ggraphics
{

	class Material
	{

	public:
		float roughness;
		float metallic;
		bool albedoIsPlainColor;
		vec3 albedoColor;
		Texture* texture;
		GLuint shaderProg;
		string name;

	};

}
