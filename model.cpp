#include "pch.h"
#include "model.h"

#include <glm/gtc/matrix_transform.hpp>


#include "camera.h"


void ggraphics::Model::render(GLRenderer& renderer, Camera& camera, GLint* lit)
{
	mat4 matWorld = translate(mat4(1), worldPosition) * scale(mat4(1), worldScale);
	for (auto mesh : meshes)
	{
		
		renderer.draw(mesh.vao, matWorld, camera.view(), camera.projection(), mesh.material->shaderProg, mesh.indices.size(), mesh.material->texture->handle, lit);
	}
}
