#pragma once
#include <vector>
#include "gmesh.h"
#include "glrenderer.h"
#include <glm/vec3.hpp>
#include <string>

using namespace  std;
namespace ggraphics
{
	class Camera;

	class Model {
	public:
		string name;
		vector<Mesh> meshes;
		vec3 worldPosition = vec3(0);
		vec3 worldScale = vec3(1);
		float yaw = 0;

		void render(GLRenderer& renderer, Camera& camera, GLint* lit);


	};
}
