#include "pch.h"
#include "texture.h"
#include <SDL.h>
#include <gl/glew.h>

ggraphics::Texture::Texture(const std::string& imageFileName, vec3 transparentColor)
{
	glGenTextures(1, &handle);
	glBindTexture(GL_TEXTURE_2D, handle);


	SDL_Surface* surface = SDL_LoadBMP(imageFileName.c_str());
	w = surface->w;
	h = surface->h;
	//if (transparentBitmap)
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, surface->w, surface->h, 0, GL_BGR, GL_UNSIGNED_BYTE, surface->pixels);
	//else
	//	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, surface->w, surface->h, 0, GL_BGR, GL_UNSIGNED_BYTE, surface->pixels);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glBindTexture(GL_TEXTURE_2D, 0);

	SDL_FreeSurface(surface);
}