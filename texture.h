#pragma once
#include <glm/glm.hpp>
#include <string>
#include <gl/glew.h>

using namespace std;
using namespace glm;


namespace ggraphics {
	
		class Texture
		{
		public:
			Texture(const std::string& fileName, vec3 transparentColor);
			GLuint handle;
			GLuint w;
			GLuint h;
			
		};
	
}